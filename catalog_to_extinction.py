#!/usr/bin/python3
"""Calculate the maximum distance along a catalog of coordinates to reach a specified maximum Av"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import json, requests
import pickle

doplot=True
doprint=False

def query(lon, lat, coordsys='gal', mode='full'):
    '''
    Send a line-of-sight reddening query to the Argonaut web server.
    
    Inputs:
      lon, lat: longitude and latitude, in degrees.
      coordsys: 'gal' for Galactic, 'equ' for Equatorial (J2000).
      mode: 'full', 'lite' or 'sfd'
    
    In 'full' mode, outputs a dictionary containing, among other things:
      'distmod':    The distance moduli that define the distance bins.
      'best':       The best-fit (maximum proability density)
                    line-of-sight reddening, in units of SFD-equivalent
                    E(B-V), to each distance modulus in 'distmod.' See
                    Schlafly & Finkbeiner (2011) for a definition of the
                    reddening vector (use R_V = 3.1).
      'samples':    Samples of the line-of-sight reddening, drawn from
                    the probability density on reddening profiles.
      'success':    1 if the query succeeded, and 0 otherwise.
      'converged':  1 if the line-of-sight reddening fit converged, and
                    0 otherwise.
      'n_stars':    # of stars used to fit the line-of-sight reddening.
      'DM_reliable_min':  Minimum reliable distance modulus in pixel.
      'DM_reliable_max':  Maximum reliable distance modulus in pixel.
    
    Less information is returned in 'lite' mode, while in 'sfd' mode,
    the Schlegel, Finkbeiner & Davis (1998) E(B-V) is returned.
    '''
    
    url = 'http://argonaut.skymaps.info/gal-lb-query-light'
    
    payload = {'mode': mode}
    
    if coordsys.lower() in ['gal', 'g']:
        payload['l'] = lon
        payload['b'] = lat
    elif coordsys.lower() in ['equ', 'e']:
        payload['ra'] = lon
        payload['dec'] = lat
    else:
        raise ValueError("coordsys '{0}' not understood.".format(coordsys))
    
    headers = {'content-type': 'application/json'}
    
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    
    try:
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        print('Response received from Argonaut:')
        print(r.text)
        raise e
    
    return json.loads(r.text)
catalog = "integrated_hii_release"
sheet = pd.ExcelFile(catalog+".xls").parse(0)
l_array = []
b_array = []
for i, (name, l, b) in enumerate(zip(sheet['name'], sheet['l'], sheet['b'])):
    l_array.append(l)
    b_array.append(b)
if doplot == True:
    Av_array = []
    d_array = []    

try:
    pickle_file = open(catalog+'_qresult.pck', 'rb')
    qresult = pickle.load(pickle_file)
    pickle_file.close()
except:
    pickle_file = open(catalog+'_qresult.pck', 'wb')
    qresult = query(l_array, b_array, coordsys='gal')
    pickle.dump(qresult, pickle_file)
    pickle_file.close()

maxAv = 5.0

if(doprint == True):
    print("i".ljust(5), "name".ljust(15), "gal_l".ljust(8), "gal_b".ljust(10), "d_kpc".ljust(6), "Av_mag".ljust(6))
for i, (name, l, b) in enumerate(zip(sheet['name'], sheet['l'], sheet['b'])):
    #print(i, name)
    #for j, (EVB, dmod) in enumerate(zip(qresult['best'][i], qresult['distmod'])):
    #    #Av = np.nanmax(qresult['best'][i])*3.1
    #    Av = EVB*3.1
    #    d = 10**(dmod/5+1)/1.0e3
    #    print("Av= %0.2f"%Av, "d(kpc) = %0.2f"%(d))

    mask = np.array(qresult['best'][i])*3.1 <= maxAv
    if len(np.array(qresult['distmod'])[mask]) > 0:
        dmod_at_maxAv = np.array(qresult['distmod'])[mask][-2]
        d = 10**(dmod_at_maxAv/5+1)/1.0e3
        Av= 3.1*np.array(qresult['best'][i])[mask][-1]
    else:
        dmod_min = np.array(qresult['distmod'])[0]
        d = 10**(dmod_min/5+1)/1.0e3
        Av = 3.1*np.array(qresult['best'][i])[0]
    if(doprint == True):
        print(str(i).ljust(5), name.ljust(15), ("%0.5f"%(l)).ljust(8), ("%0.5f"%(b)).ljust(10), ("%0.3f"%(d)).ljust(6), ("%0.3f"%(Av)).ljust(6))
    if doplot == True:
        Av_array.append(Av)
        d_array.append(d)
 

if doplot == True:
    import matplotlib.pyplot as plt
    ymax = 5.2
    xmax = 15
    plt.fill_between([0,xmax],[0,0], [3,3], facecolor="green", alpha=0.3)
    plt.fill_between([0,xmax],[3,3], [4,4], facecolor="yellow", alpha=0.3)
    plt.fill_between([0,xmax],[4,4], [ymax,ymax], facecolor="red", alpha=0.3)
    plt.scatter(d_array, Av_array)
    plt.xlabel("d(kpc) @ Av $\leq$ 5.0 || max(d)")
    plt.ylabel("Av(mag)")
    plt.xlim(0,xmax)
    plt.ylim(0, ymax)
    plt.title("Lines of site toward MW HII region\n Distance where Av=5.0 or max distance traced by stars along los")
    plt.savefig("MW_HII_region_line_of_site_extincion.png")

exit()
